A blog all about Hollister CA Home Prices & Home Values.

* [Did Home Prices in Hollister CA Rise in 2016?](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/hollister-ca-home-prices-home-values/raw/master/notebooks/Home%20Prices%20in%20Hollister%20CA%202016.ipynb)
* [Hollister CA Rental Prices through Jan 2017](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/hollister-ca-home-prices-home-values/raw/master/notebooks/Hollister%20CA%20Rental%20Prices%20through%20Jan%202017.ipynb)
* [What Percentage of Sales were Foreclosures in Hollister CA?](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/hollister-ca-home-prices-home-values/raw/master/notebooks/What%20Percentage%20of%20Sales%20were%20Foreclosures%20in%20Hollister%20CA.ipynb)
* [Are Hollister CA Homes Increasing or Decreasing in Value?](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/hollister-ca-home-prices-home-values/raw/master/notebooks/Are%20Hollister%20CA%20Homes%20Increasing%20in%20Value%20or%20Decreasing%20in%20Value.ipynb)

